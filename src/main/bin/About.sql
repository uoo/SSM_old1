-- auto Generated on 2017-03-09 15:43:47 
-- DROP TABLE IF EXISTS `about`; 
CREATE TABLE `about`(
    `id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `title` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'title',
    `classify` INT (11) NOT NULL DEFAULT -1 COMMENT 'classify',
    `image` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'image',
    `content` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'content',
    `description` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'description',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`about`';
