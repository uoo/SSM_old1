-- 1.创建用户信息表
DROP TABLE IF EXISTS user_info;
CREATE TABLE `user_info` (
  `user_id`     BIGINT(15)      NOT NULL AUTO_INCREMENT
  COMMENT '用户ID',
  `username`    VARCHAR(30)     NOT NULL
  COMMENT '用户名',
  `password`    VARCHAR(50)     NOT NULL
  COMMENT '密码',
  `real_name`   VARCHAR(10)     NOT NULL
  COMMENT '真实姓名',
  `sex`         ENUM ('男', '女') NOT NULL
  COMMENT '性别',
  `phone`       VARCHAR(20)     NOT NULL
  COMMENT '电话',
  `email`       VARCHAR(128)    NOT NULL
  COMMENT '电子邮箱',
  `address`     VARCHAR(256)             DEFAULT NULL
  COMMENT '住址',
  `head_pic`    VARCHAR(100)             DEFAULT NULL
  COMMENT '用户图像',
  `role_id`     BIGINT(15)      NOT NULL DEFAULT '0'
  COMMENT '用户角色ID',
  `login_time`  TIMESTAMP       NULL     DEFAULT NULL
  COMMENT '上次登录时间',
  `create_time` TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  COMMENT '创建时间',
  `update_time` TIMESTAMP       NULL     DEFAULT NULL
  COMMENT '修改时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_id` (`user_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户信息表';


-- 2.创建学员信息表
DROP TABLE IF EXISTS `stu_info`;
CREATE TABLE `stu_info` (
  `stu_id`         BIGINT(15)  NOT NULL AUTO_INCREMENT
  COMMENT '学员ID',
  `stu_name`       VARCHAR(10) NOT NULL
  COMMENT '学员姓名',
  `edu_background` VARCHAR(10) NOT NULL
  COMMENT '学历',
  `status`         VARCHAR(10) NOT NULL
  COMMENT '目前状态',
  `major`          VARCHAR(10) NOT NULL
  COMMENT '专业',
  `course_id`      BIGINT(15)  NOT NULL
  COMMENT '课程ID',
  PRIMARY KEY (`stu_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '学员信息表';


-- 3.创建就业信息表
DROP TABLE IF EXISTS `emp_info`;
CREATE TABLE `emp_info` (
  `emp_id`     BIGINT(15)    NOT NULL AUTO_INCREMENT
  COMMENT '受雇ID',
  `stu_id`     BIGINT(15)    NOT NULL
  COMMENT '学员ID',
  `enterprise` VARCHAR(30)   NOT NULL
  COMMENT '入职企业',
  `mon_salary` DOUBLE(16, 4) NOT NULL
  COMMENT '月薪',
  `address`    VARCHAR(60)   NOT NULL
  COMMENT '地点',
  `hire_date`  DATETIME      NOT NULL
  COMMENT '受雇日期',
  PRIMARY KEY (`emp_id`),
  KEY `emp_id` (`emp_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '`就业信息表`';


