-- auto Generated on 2017-03-09 19:08:42 
-- DROP TABLE IF EXISTS `course`; 
CREATE TABLE `course`(
    `c_id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'cId',
    `cs_id` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'csId',
    `c_name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'cName',
    `begins_date` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'beginsDate',
    `status` INT (11) NOT NULL DEFAULT -1 COMMENT 'status',
    `capacity` INT (11) NOT NULL DEFAULT -1 COMMENT 'capacity',
    `current_num` INT (11) NOT NULL DEFAULT -1 COMMENT 'currentNum',
    PRIMARY KEY (`c_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`course`';
