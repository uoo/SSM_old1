-- 用户信息表批量插入测试数据
INSERT INTO user_info (username, password, real_name, sex, phone, email, address, head_pic, role_id, login_time, create_time, update_time)
VALUES
  ('coffee377', 'wuyujie', '吴玉杰', '男', '13956945898', 'coffee377@dingtalk.com', '安徽省合肥市高新区', 'upload/user.jpg', -1,
                NULL, '2016-12-29 12:38:58', NULL);
INSERT INTO user_info (username, password, real_name, sex, phone, email, role_id, create_time)
VALUES
  ('demo', '123456', 'Demo', '男', '12345678901', 'demo@163.com', 0, '2017-1-2 21:31:09');
DELIMITER //
CREATE PROCEDURE batch_insert()
  BEGIN
    DECLARE num INT;
    SET num = 1;
    WHILE num < 10 DO
      INSERT INTO user_info (username, password, real_name, sex, phone, email, role_id, create_time)
      VALUES
        (concat('demo', num), '123456', concat('Demo', num), '男', '12345678901', concat('demo', num, 'demo@163.com'), 0,
         now());
      SET num = num + 1;
    END WHILE;
  END //
CALL batch_insert;
DROP PROCEDURE IF EXISTS batch_insert;
