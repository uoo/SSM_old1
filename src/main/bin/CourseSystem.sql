-- auto Generated on 2017-03-09 17:49:02 
-- DROP TABLE IF EXISTS `course_system`; 
CREATE TABLE `course_system`(
    `cs_id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'csId',
    `cs_name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'csName',
    `nick_name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'nickName',
    `description` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'description',
    PRIMARY KEY (`cs_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`course_system`';
