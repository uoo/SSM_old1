CREATE TABLE img(
i_id int PRIMARY KEY ,
sort VARCHAR (10),
img_src VARCHAR (1000),
description VARCHAR(100)
);


CREATE TABLE course_sys(
cs_id INT PRIMARY KEY ,
cs_name VARCHAR(20),
  description varchar(10000)
);

CREATE TABLE course (
  c_id INT PRIMARY KEY,
  cs_name VARCHAR(20)  ,
  c_name     VARCHAR(20),
  begin_date DATE,
  state      INT,
  peo_num    INT,
  have_peo_num INT
);
# drop TABLE course;
# drop TABLE course_sys;
# drop TABLE img;