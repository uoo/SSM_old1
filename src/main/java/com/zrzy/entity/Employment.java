package com.zrzy.entity;

import java.util.Date;

/**
 * 就业信息表
 *
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/07 17:06
 */
public class Employment {
    /**
     * 受雇 ID
     */
    private Long empId;

    /**
     * 学员 ID
     */
    private Long stuId;

    /**
     * 入职企业
     */
    private String enterprise;

    /**
     * 月薪
     */
    private Double monSalary;

    /**
     * 地点
     */
    private String address;

    /**
     * 受雇日期
     */
    private Date hireDate;

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public Long getStuId() {
        return stuId;
    }

    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Double getMonSalary() {
        return monSalary;
    }

    public void setMonSalary(Double monSalary) {
        this.monSalary = monSalary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    @Override
    public String toString() {
        return "Employment{" +
                "empId=" + empId +
                ", stuId=" + stuId +
                ", enterprise='" + enterprise + '\'' +
                ", monSalary=" + monSalary +
                ", address='" + address + '\'' +
                ", hireDate=" + hireDate +
                '}';
    }
}
