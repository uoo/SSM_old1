package com.zrzy.entity;

/**
 * 常量（数据字典）数据库表
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/09 13:17
 */
public class Dictionary {
    /**
     * 常量 ID
     */
    private Long id;

    /**
     * 常量分类
     */
    private String classify;

    /**
     * 常量名称
     */
    private String name;

    /**
     * 常量值
     */
    private Integer value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
