package com.zrzy.entity;

public class News {
    private Integer newsId;

    private String newTitle;

    private String newsImg;

    private String newsDesription;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getNewTitle() {
        return newTitle;
    }

    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle == null ? null : newTitle.trim();
    }

    public String getNewsImg() {
        return newsImg;
    }

    public void setNewsImg(String newsImg) {
        this.newsImg = newsImg == null ? null : newsImg.trim();
    }

    public String getNewsDesription() {
        return newsDesription;
    }

    public void setNewsDesription(String newsDesription) {
        this.newsDesription = newsDesription == null ? null : newsDesription.trim();
    }
}