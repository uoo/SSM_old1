package com.zrzy.entity;

public class Img {
    private Integer iId;

    @Override
    public String toString() {
        return "Img{" +
                "iId=" + iId +
                ", sort='" + sort + '\'' +
                ", imgSrc='" + imgSrc + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    private String sort;

    private String imgSrc;

    private String description;

    public Integer getiId() {
        return iId;
    }

    public void setiId(Integer iId) {
        this.iId = iId;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort == null ? null : sort.trim();
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc == null ? null : imgSrc.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}