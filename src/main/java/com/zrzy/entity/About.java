package com.zrzy.entity;

/**
 * 关于蔻丁
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/08 22:19
 */
public class About {
    /**
     * 关于ID
     */
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 分类
     * news 新闻 0
     * story 故事 1
     * info 行业动态 2
     */
    private Integer classify;
    /**
     * 图像
     */
    private String image;
    /**
     * 内容
     */
    private String content;
    /**
     * 描述
     */
    private String description;

    public About(Long id, String title, Integer classify, String image, String description) {
        this.id = id;
        this.title = title;
        this.classify = classify;
        this.image = image;
        this.description = description;
    }

    public About() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getClassify() {
        return classify;
    }

    public void setClassify(Integer classify) {
        this.classify = classify;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "About{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", classify=" + classify +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
