package com.zrzy.entity;

/**
 * 课程体系
 */
public class CourseSystem {

    /**
     * 课程体系 ID
     */
    private Integer csId;

    /**
     * 课程体系名称
     */
    private String csName;

    /**
     * 课程体系匿名
     */
    private String nickName;

    /**
     * 课程体系描述
     */
    private String description;

    public Integer getCsId() {
        return csId;
    }

    public void setCsId(Integer csId) {
        this.csId = csId;
    }

    public String getCsName() {
        return csName;
    }

    public void setCsName(String csName) {
        this.csName = csName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CourseSystem{" +
                "csId=" + csId +
                ", csName='" + csName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}