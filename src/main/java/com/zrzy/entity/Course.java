package com.zrzy.entity;

import java.util.Date;

/**
 * 开班信息（课程表）
 */
public class Course {
    /**
     * 课程 ID
     */
    private Integer cId;

    /**
     * 课程体系 ID
     */
    private String csId;

    /**
     * 课程名称
     */
    private String cName;

    /**
     * 开课日期
     */
    private Date beginsDate;

    /**
     * 班级状态（-1：禁用   0：开班    1：抢座    2：爆满）
     */
    private Integer status;

    /**
     * 班级容纳人数
     */
    private Integer capacity;

    /**
     * 班级当前人数
     */
    private Integer currentNum;

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getCsId() {
        return csId;
    }

    public void setCsId(String csId) {
        this.csId = csId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public Date getBeginsDate() {
        return beginsDate;
    }

    public void setBeginsDate(Date beginsDate) {
        this.beginsDate = beginsDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getCurrentNum() {
        return currentNum;
    }

    public void setCurrentNum(Integer currentNum) {
        this.currentNum = currentNum;
    }

    @Override
    public String toString() {
        return "Course{" +
                "cId=" + cId +
                ", csId='" + csId + '\'' +
                ", cName='" + cName + '\'' +
                ", beginsDate=" + beginsDate +
                ", status=" + status +
                ", capacity=" + capacity +
                ", currentNum=" + currentNum +
                '}';
    }
}