package com.zrzy.controller;

import com.zrzy.entity.About;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/07 21:41
 */
@Controller
@RequestMapping(path = "/")
public class IndexController {
    /**
     * 网站主界面
     *
     * @return 模板文件名
     */
    @RequestMapping(path = {"/", "index.html"})
    public ModelAndView indexPage() {
        ModelAndView mv = new ModelAndView("index");
        // TODO: 2017/3/7 0007 22:16 添加首页业务数据
        About about1 = new About(1L,"喜讯：深圳JavaEE首期班毕业两周100%就业 平均薪资12729元",0,"picture/carousel_01.jpg",null);
        About about2 = new About(2L,"重磅!千锋Python培训耀世起航 首期班钜惠2000还送树莓派",0,"picture/carousel_02.jpg",null);
        About about3 = new About(3L,"爱人者 人恒爱之 救救李乐英那患有白血病爸爸的捐款倡议书",0,"picture/carousel_03.jpg",null);
        About about4 = new About(4L,"千锋课程钜惠来袭 史无前例席卷全国",0,"picture/carousel_04.jpg",null);
        About about5 = new About(5L,"喜讯：千锋课程钜惠来袭 史无前例席卷全国 平均薪资12729元",0,"picture/carousel_01.jpg",null);
        List<About> carouse = new ArrayList<>();
        carouse.add(about1);
        carouse.add(about2);
        carouse.add(about3);
        carouse.add(about4);
        carouse.add(about5);

        mv.addObject("title", "首页");
        //头部轮播数据
        mv.addObject("carouse", carouse);
        return mv;
    }

    /**
     * 公用头部
     *
     * @return 模板文件名
     */
    @RequestMapping("header")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("common/header");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 公用底部
     *
     * @return 模板文件名
     */
    @RequestMapping("footer")
    public ModelAndView footer() {
        ModelAndView mv = new ModelAndView("common/footer");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * Android 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("/course/android.html")
    public ModelAndView ad() {
        ModelAndView mv = new ModelAndView("course/android");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
    /**
     * h5 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("/course/html5.html")
    public ModelAndView htm() {
        ModelAndView mv = new ModelAndView("course/html5");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
    /**
     * UI 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("/course/ui.html")
    public ModelAndView ui() {
        ModelAndView mv = new ModelAndView("course/ui");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
    /**
     * php 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("/course/php.html")
    public ModelAndView php() {
        ModelAndView mv = new ModelAndView("course/php");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
    /**
     * java 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("/course/java.html")
    public ModelAndView java() {
        ModelAndView mv = new ModelAndView("course/java");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
    /**
     * 大数据主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("/course/big_data.html")
    public ModelAndView big() {
        ModelAndView mv = new ModelAndView("course/big_data");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 讲师团队主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("team/teacher.html")
    public ModelAndView teachTeam() {
        ModelAndView mv = new ModelAndView("teacher");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 就业喜报
     *
     * @return 模板文件名
     */
    @RequestMapping("employment/carmakers.html")
    public ModelAndView employmentCarmakers() {
        ModelAndView mv = new ModelAndView("teacher");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 学生感言主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("student/testimonials.html")
    public ModelAndView testimonials() {
        ModelAndView mv = new ModelAndView("testimonials");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 关于蔲丁主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("about/coding.html")
    public ModelAndView aboutCoding() {
        ModelAndView mv = new ModelAndView("about_coding");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
}
