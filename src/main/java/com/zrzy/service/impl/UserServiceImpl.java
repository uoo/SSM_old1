package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.UserDao;
import com.zrzy.entity.User;
import com.zrzy.service.UserService;
import com.zrzy.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/03 12:51
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;


    @Override
    public boolean existUser(String username) {
        return userDao.existUsername(username) > 0;
    }

    @Override
    public int verify(String username, String password) {
        if (!existUser(username)) {
            return LOGIN_ERROR;
        } else {
            User loginUser = login(username, password);
            if (null != loginUser) {
                return LOGIN_SUCCESS;
            }
            return LOGIN_FAIL;
        }
    }

    @Override
    public User login(String username, String password) {
        return userDao.login(username, password);
    }

    @Override
    public void updateLoginTime(Long userId) {
        userDao.updateLoginTime(userId);
    }

    @Override
    public void deleteById(List<Long> ids) {
        userDao.deleteById(ids);
    }

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }

    @Override
    public List<User> queryAsList(int pageNumber, int pageSize) {
        return userDao.queryAsList((pageNumber - 1) * pageSize, pageSize);
    }

    @Override
    public PageBean<User> queryForPage(int pageNumber, int pageSize) {
        int totalSize = getAll().size();
        List<User> entityList = queryAsList(pageNumber, pageSize);
        int totalPage = PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(entityList, totalSize, totalPage, pageNumber, pageSize);
    }
}
