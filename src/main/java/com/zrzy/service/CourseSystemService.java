package com.zrzy.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.zrzy.entity.CourseSystem;
import com.zrzy.dao.CourseSystemDao;

@Service
public interface CourseSystemService{
    //查
    List<CourseSystem> queryCourseSystem();
    //增
    int insertCourseSystem(@Param("courseSystem") CourseSystem courseSystem);
    //更
    int updateCourseSystem(@Param("courseSystem") CourseSystem courseSystem);
    //删
    int deleteCourseSystemById(int id);
}
