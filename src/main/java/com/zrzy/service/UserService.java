package com.zrzy.service;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    /**用户名不存在*/
    int LOGIN_ERROR = 10000;

    /**用户名与密码不匹配*/
    int LOGIN_FAIL = 10001;

    /**用户名与密码匹配成功*/
    int LOGIN_SUCCESS = 10002;


    boolean existUser(String username);

    /**
     * 校验用户
     *
     * @param username 用户名
     * @param password 密码
     * @return int  状态码
     */
    int verify(String username, String password);

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @return 登录成功返回当前登录用户，否则
     */
    User login(String username, String password);


    void updateLoginTime(Long userId);

    void deleteById(List<Long> ids);

    List<User> getAll();

    List<User> queryAsList(int pageNumber, int pageSize);

    PageBean<User> queryForPage(int pageNumber, int pageSize);
}
