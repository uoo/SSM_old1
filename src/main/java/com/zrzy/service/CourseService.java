package com.zrzy.service;

import com.zrzy.entity.Course;

import java.util.List;

/**
 * Created by gtz on 2017/3/8.
 */
public interface CourseService {
    List<Course> queryAllCourse();
}

