package com.zrzy.service;

import com.zrzy.entity.About;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AboutService{

    int insert(About pojo);

    int insertSelective(About pojo);

    int insertList(List<About> pojos);

    int update(About pojo);
	
}
