package com.zrzy.dao;

import com.zrzy.entity.Img;

public interface ImgDao {
    int deleteByPrimaryKey(Integer iId);

    int insert(Img record);

    int insertSelective(Img record);

    Img selectByPrimaryKey(Integer iId);

    int updateByPrimaryKeySelective(Img record);

    int updateByPrimaryKey(Img record);
}