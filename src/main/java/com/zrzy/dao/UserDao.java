package com.zrzy.dao;

import com.zrzy.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao {
    int existUsername(@Param("param") String username);

    void updateLoginTime(@Param("param") Long userId);

//    int save(@Param("pojo") User user);

    void deleteById(@Param("ids") List<Long> userIds);

//    void update(@Param("user") User user);

    void findById(@Param("id") Long userId);

    User login(@Param("username") String username, @Param("password") String password);

    List<User> queryAsList(@Param("o") int offset,@Param("s") int size);
//    int insertSelective(@Param("pojo") User pojo);
//
//    int insertList(@Param("pojos") List<User> pojo);

    List<User> getAll();
//    int update(@Param("pojo") User pojo);
}
