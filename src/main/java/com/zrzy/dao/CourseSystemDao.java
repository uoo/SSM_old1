package com.zrzy.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.zrzy.entity.CourseSystem;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseSystemDao {
    //查
    List<CourseSystem> queryCourseSystem();
    //增
    int insertCourseSystem(CourseSystem courseSystem);
    //更
    int updateCourseSystem(@Param("cs") CourseSystem courseSystem);
    //删
    int deleteCourseSystemById(int id);
}
