package com.zrzy.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.zrzy.entity.About;
import org.springframework.stereotype.Repository;

@Repository
public interface AboutDao {
    /*插入一条新闻*/
    int insert(About about);

    /*根据id删除一条新闻*/
    int deleteById(Long id);

    /*根据id更新一条新闻*/
    int updateById(About about);

    /*根据分类查询新闻*/
    List<About> findByClassify(Integer classify);
}
