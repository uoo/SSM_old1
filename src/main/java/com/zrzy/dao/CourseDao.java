package com.zrzy.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.zrzy.entity.Course;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseDao {
    /*插入一条课程信息*/
    int insert(Course course);

    /*根据id删除一条课程信息*/
    int deleteById(Integer cId);

    /*根据id更新课程信息*/
    int updateByCId(Course course);

    /*根据课程体系csId查询课程信息*/
    List<Course> findByCsId(Integer csId);
}
