<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2017/3/8
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<meta charset="UTF-8">
<title>蔻丁教育 - 中国iOS培训|Android培训|HTML5培训|UI培训|PHP培训|Java培训|大数据|VR培训开</title>
<meta name="keywords" content="Android培训,安卓培训,安卓开发培训,Android开发培训,HTML5培训,UI培训,PHP培训,Java培训,JavaEE培训,大数据培训"/>
<meta name="description"
      content="中国IT职业教育领先品牌,Android培训、HTML5培训、UI培训、PHP培训、Java培训、大数据培训、HTML5开发培训视频,寇丁教育长期坚持用“良心做教育”的理念。"/>
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<!--<link rel="stylesheet" href="css/base_index.css"/>
<link rel="stylesheet" href="css/base_bottom.css"/>-->
<link rel="icon" href="#" type="image/x-icon"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/xygy20161202.css">
<style type="text/css">
    DIV.digg {
        PADDING-RIGHT: 3px;
        PADDING-LEFT: 3px;
        PADDING-BOTTOM: 3px;
        MARGIN: 3px;
        PADDING-TOP: 3px;
        TEXT-ALIGN: center
    }

    DIV.digg A {
        BORDER-RIGHT: #aaaadd 1px solid;
        PADDING-RIGHT: 5px;
        BORDER-TOP: #aaaadd 1px solid;
        PADDING-LEFT: 5px;
        PADDING-BOTTOM: 2px;
        MARGIN: 2px;
        BORDER-LEFT: #aaaadd 1px solid;
        COLOR: #808080;
        PADDING-TOP: 2px;
        BORDER-BOTTOM: #aaaadd 1px solid;
        TEXT-DECORATION: none
    }

    DIV.digg A:hover {
        BORDER-RIGHT: #808080 1px solid;
        BORDER-TOP: #808080 1px solid;
        BORDER-LEFT: #808080 1px solid;
        COLOR: #000;
        BORDER-BOTTOM: #808080 1px solid
    }

    DIV.digg A:active {
        BORDER-RIGHT: #808080 1px solid;
        BORDER-TOP: #808080 1px solid;
        BORDER-LEFT: #808080 1px solid;
        COLOR: #000;
        BORDER-BOTTOM: #808080 1px solid
    }

    DIV.digg SPAN.current {
        BORDER-RIGHT: #808080 1px solid;
        PADDING-RIGHT: 5px;
        BORDER-TOP: #808080 1px solid;
        PADDING-LEFT: 5px;
        FONT-WEIGHT: bold;
        PADDING-BOTTOM: 2px;
        MARGIN: 2px;
        BORDER-LEFT: #808080 1px solid;
        COLOR: #fff;
        PADDING-TOP: 2px;
        BORDER-BOTTOM: #808080 1px solid;
        BACKGROUND-COLOR: #808080
    }

    DIV.digg SPAN.disabled {
        BORDER-RIGHT: #eee 1px solid;
        PADDING-RIGHT: 5px;
        BORDER-TOP: #eee 1px solid;
        PADDING-LEFT: 5px;
        PADDING-BOTTOM: 2px;
        MARGIN: 2px;
        BORDER-LEFT: #eee 1px solid;
        COLOR: #ddd;
        PADDING-TOP: 2px;
        BORDER-BOTTOM: #eee 1px solid
    }
</style>
</head>
<body>
<jsp:include page="/head"/>

<div class="banner">
</div>
<div class="video" id="vtab">
   <%-- <div class="video_tab">
        <span class="active" id="2">Android</span>
        <span class="" id="13">HTML5</span>
        <span class="" id="19">UI设计</span>
        <span class="" id="1">iOS</span>
    </div>--%>
    <div class="base">
        <input type="hidden" value="#" id="url">
        <div class="tab_nr clear active">
            <%--<div class="video_tab_left">
                <ul class="video_time">
                    <li>
                        <i class="active">2016</i>
                        <div class="active" style="height: 458.33333333333px;opacity:1">
                            <span>11月</span><span>10月</span><span>9月</span><span>8月</span><span>7月</span><span>6月</span><span>5月</span><span>4月</span><span>3月</span><span>2月</span><span>1月</span>
                        </div>
                    </li>
                    <li>
                        <i class="">2015</i>
                        <div style="height:0;opacity:0;">
                            <span>12月</span><span>11月</span><span>10月</span><span>9月</span><span>8月</span><span>7月</span><span>6月</span><span>5月</span><span>4月</span><span>3月</span><span>2月</span><span>1月</span>
                        </div>
                    </li>
                    <li>
                        <i class="">2014</i>
                        <div style="height:0;opacity:0;">
                            <span>12月</span><span>11月</span><span>10月</span><span>9月</span><span>8月</span><span>7月</span><span>6月</span><span>5月</span><span>4月</span><span>3月</span><span>2月</span><span>1月</span>
                        </div>
                    </li>
                </ul>
            </div>--%>
            <div class="video_tab_r">
                <ul class="active">
                    <li class="clear active">
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58538f5aee21d.jpg" height="300" width="422">
                            <h6>邓同学-广州1603期-Android</h6>
                            <span>就业薪水：10000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    在千锋的这段学习之旅，可以说是我人生中最、最、最努力去学习的一次，同学们都只看到我在努力的学习，说我好像从来都不会累，像铁打的一样......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58538f901d810.jpg" height="300" width="422">
                            <h6>王同学-杭州1603期-Android</h6>
                            <span>就业薪水：10000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    在千锋四个月的学习，是我人生宝贵的财富。我工作需要的技术，千锋都教过，特别是对应届生而言，这种经验是最宝贵的。因为我有一些经验就成......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58538fba6b2ba.jpg" height="300" width="422">
                            <h6>盛同学-武汉1604期-Android</h6>
                            <span>就业薪水：8000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    来千锋之前，我没有从事过计算机相关的工作，曾经创业过，但是没有取得很好的成绩。在朋友推荐下，我来到武汉千锋，这里成为我新的起点。对......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/5853901d14ee8.jpg" height="300" width="422">
                            <h6>程同学-郑州1604期-Android</h6>
                            <span>就业薪水：10000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays//video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    作为一个应届毕业生，在即将毕业的时候我陷入了深度迷茫，参加了很多招聘会却没有找到令自己满意的工作，千锋的学员（我的朋友）向我推荐来......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/5853905218e6d.jpg" height="300" width="422">
                            <h6>谷同学-郑州1603期-Android</h6>
                            <span>就业薪水：13000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    之前有过创业失败的经历，当时感觉彷徨无助，偶然的一次机会了解到千锋，经过多方比较之后选择千锋。就学习来说，每一个阶段的老师都非常专......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58539081095fc.jpg" height="300" width="422">
                            <h6>于同学-郑州1603期-Android</h6>
                            <span>就业薪水：11000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    结婚生子后我想找一份稳定能养家的工作，本专业的工作薪资低就不再考虑了。表弟从千锋毕业后高薪就业，了解千锋后我决定到这里学习，千锋生......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/585388f796777.jpg" height="300" width="422">
                            <h6>陈同学-北京1605期-Android</h6>
                            <span>就业薪水：11000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    来千锋之前，面临着大四非常严峻的就业形势。在老师的推荐下我来到千锋，开始了四个月的充实学习，在这里感觉又回到了高考状态，千锋老师对......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58538a2cdf4be.jpg" height="300" width="422">
                            <h6>潘同学-北京1517期-Android</h6>
                            <span>就业薪水：13000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    好玩导致我学习成绩一般，加之没有特长和优势，毕业后找了一份生产流水线工作，枯燥乏味，更谈不上前途和发展空间。看准了移动互联网的良好......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58538a8dbda45.jpg" height="300" width="422">
                            <h6>江同学-广州1509期-Android</h6>
                            <span>就业薪水：8000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    很高兴能够在这里，和大家分享我的学习和工作经验。我觉得，大学学到的东西很散、很杂，根本没有激发自己的兴趣，没有很好去发展个人技能方......</p>
                            </div>
                        </a>
                        <a href="#" target="_blank" class="video_list">
                            <img src="${pageContext.request.contextPath}/image/studentsSays/58538c7a69ee2.jpg" height="300" width="422">
                            <h6>陈同学-武汉1504期-Android</h6>
                            <span>就业薪水：8000</span>
                            <img src="${pageContext.request.contextPath}/image/studentsSays/video_ico2.png" height="70" width="68" class="video_btn">
                            <div>
                                <em></em>
                                <i>学员感言视频</i>
                                <p style="text-align: left">
                                    建议大家找到一个适合自己的学习方法，毕竟四个月的学习内容很多，接受起来需要一定的时间，不光是课上老师传授的内容，课外也需要时间吸收......</p>
                            </div>
                        </a>
                        </foreach>
                    </li>
                </ul>
                <div class="digg">
                    <span class="disabled">&lt; </span><span class="current">1</span>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <a href="#">5</a>
                    <a href="#">6</a>
                    <a href="#">&gt; </a>
                </div>
            </div>
        </div>
    </div>
    <div class="main3">
        <div class="base">
            <h3><span>我思，我感，</span>故我在</h3>
            <p>你只羡慕我的高薪，却没看到我的艰辛<br/> 你有你的考研，我有我的编程语言
                <br/> 你否定我看似艰辛不稳定的现在，我决定我骄傲潇洒有成就感的未来
                <br/> 你嘲笑我不如盖茨和乔布斯，不配去创造，我可怜你总是啃老
                <br/> 你可以轻视我的年少轻狂，我会证明这是谁的时代
                <br/> 编程注定是漫长的旅行，路上少不了纠结和质疑，但那又怎样
                <br/> 哪怕Bug无数，也要用代码畅写人生
                <br/> 我是蔻丁学员，我为蔻丁代言！
            </p>
        </div>
    </div>
    <div class="main4">
        <div class="base">
            <h3><span>蔻丁</span>不只是一家培训机构 而是通往未来职场的门</h3>
            <ul>
                <li class="fl">
                    <a href="#" target="_blank"><span></span><span>了解完整课程体系</span></a>
                </li>
                <li class="fl">
                    <a href="#" target="_blank"><span></span><span>了解更多学员感悟</span></a>
                </li>
                <li class="fl">
                    <a href="#" target="_blank"><span></span><span>查看学员任职名企</span></a>
                </li>
                <li class="fl">
                    <a href="#" target="_blank"><span></span><span>查看往期就业薪资</span></a>
                </li>
                <li class="fl">
                    <a href="#" target="_blank"><span></span><span>为什么人人称赞</span></a>
                </li>
                <li class="fl">
                    <a href="#" target="_blank"><span></span><span>更多高薪“内幕”</span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="bottom_part2">

    </div>
    <div class="bottom_part3">

    </div>
       <jsp:include page="/footer"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/xygy20161202.js"></script>
    <script>
        $(function () {
            var url = $('#url').val();
            var h_ = $('.video_tab_r').height();
            $('.video_time').height(h_ + 72);
            if (h_ < 440) {
                $('.video_time').height(650)
            }
            $(".video_tab span").each(function (big_index) {
                $(this).click(function () {
                    var subject = $(this).attr('id');
                    setTimeout(location.href = url + '?subject=' + subject + '#vtab', 60);
                })
            })
            $(".video_time i").each(function (i_index) {
                $(this).click(function () {
                    var year = $(this).text();
                    var subject = $('.video_tab').children("span[class='active']").attr('id');
                    setTimeout(location.href = url + '?subject=' + subject + '&year=' + year + '#vtab', 30);
                })
            })
            $(".video_time span").each(function (span_index) {
                $(this).click(function () {
                    var month_ = $(this).text();
                    var month = parseInt(month_);
                    var subject = $('.video_tab').children("span[class='active']").attr('id');
                    var year = $('.video_time').children('li').children("i[class='active']").text();
                    setTimeout(location.href = url + '?subject=' + subject + '&year=' + year + '&month=' + month + '#vtab', 30);
                })
            })

        })
    </script>
</body>
</html>
