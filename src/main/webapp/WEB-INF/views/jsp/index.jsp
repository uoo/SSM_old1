<%@ page language="java" pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>SSM_DEMO</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico">
    <style type="text/css">
        h2{
            text-align: center;
            margin:100px 0;
        }

    </style>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
</head>

<body>
<jsp:include page="/header"/>
<h2>Maven + Spring + Spring MVC + MyBatis + FreeMarker 项目搭建</h2>

<h2>
    <a href="${pageContext.request.contextPath}/login.html">登录</a>
</h2>
<jsp:include page="/footer"/>
</body>
</html>
