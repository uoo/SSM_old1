<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2017/3/8
  Time: 13:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/base_index.css"/>
</head>

<body>
<div class="nav_main">
    <div class="nav_fixed">
        <div class="base clear">
            <div class="nav_left">
                <a href="#" target="_blank" title="蔻丁教育"><img src="${pageContext.request.contextPath}/image/head/logo.png" height="39" width="163"></a>
                <div class="xq">
                    <i>合肥</i>
                    <div class="xq_tab">
                    </div>
                </div>
            </div>
            <div class="nav_right">
                <a href="#" target="_blank" class="nav_index nav_on nav">首页</a>
                <span class="nav">课程
				</span>
                <a href="#" target="_blank" class="nav">讲师团队</a>
                <a href="#" target="_blank" class="nav" style="color: red;">就业喜报</a>
                <a href="#" target="_blank" class="nav">学员感言</a>
                <a href="#" target="_blank" class="nav">关于蔻丁</a>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.0.js"></script>
<!--<script src="head_php_new.js"></script>-->
<script type="text/javascript">
    $(function() {
        $("p.hc_floatleft").html("中国IT职业教育领先品牌-寇丁教育");
        $("strong.floatLeft").html("中国IT职业教育领先品牌-寇丁教育");
        $(".nav_main .nav_fixed").append('<div class="class170109">'+
            '<div class="basebase">'+
            '<div class="base clear">'+
            '<a href="#" target="_blank">'+
            '<img src="/demo/image/head/ad_ico.png" width="63" height="73" />'+
            '<u>Android</u>'+
            '</a>'+
            '<a href="#" target="_blank">'+
            '<img src="/demo/image/head/h5_ico.png" width="63" height="73" />'+
            '<u>HTML5</u>'+
            '</a>'+
            '<a href="#" target="_blank">'+
            '<img src="/demo/image/head/ui_ico.png" width="63" height="73" />'+
            '<u>UI</u>'+
            '</a>'+
            '<a href="#" target="_blank">'+
            '<img src="/demo/image/head/php_ico.png" width="63" height="73" />'+
            '<u>PHP</u>'+
            '</a>'+
            '<a href="#" target="_blank">'+
            '<img src="/demo/image/head/java_ico.png" width="63" height="73" />'+
            '<u>JavaEE</u>'+
            '</a>'+
            '<a href="#" target="_blank">'+
            '<img src="/demo/image/head/big_ico.png" width="63" height="73" />'+
            '<u>大数据开发</u>'+
            '</a>'+
            '</div>'+
            '</div>'+
            '</div>')

    })
</script>
<script type="text/javascript">
    /*课程*/
    $(function(){
        $(".nav_right .nav").mouseout(function(){
            $(".nav_right .nav").eq(0).addClass('nav_on').siblings().removeClass('nav_on');
        })
        $(".nav_right span,.class170109").hover(function(){
            $(".class170109").addClass('on');
            $(".nav_right span").addClass('on');
            $(".nav_right .nav").eq(0).removeClass('nav_on');
        },function(){
            $(".class170109").removeClass('on');
            $(".nav_right span").removeClass('on');
            $(".nav_right .nav").eq(0).addClass('nav_on');
        })
        $(".nav_right .nav").each(function(nav_index){
            $(this).mouseover(function(){
                $(this).addClass('nav_on').siblings().removeClass('nav_on');
            })
        })
    })

</script>
</html>
