<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="${webRoot}/favicon.ico">
	<link href="${webRoot}/css/bootstrap.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="${webRoot}/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="${webRoot}/layui/css/layui.css"/>

	<title>${sysName}</title>
	<style type="text/css">
		body {
			background: #fff;
			width: 100%;
			overflow: hidden;
		}

		#frame-header {
			height: 60px;
			background: #438EB9;
		}

		#frame-body {
			width: 100%;
			height: calc(100% - 60px);
		}

		#frame-menu {
			float: left;
			width: 200px;
			height: 100%;
			border-right: 1px solid #ccc;
			overflow: auto;
		}

		#frame-content {
			float: left;
			width: calc(100% - 200px);
			height: 100%;
		}

		#nav_content {
			widows: 100;
			height: 30px;
			border-bottom: 1px solid #ccc;
		}

		#content {
			width: calc(100%);
			height: calc(100% - 30px);
			border: none;
		}

		#frame-banner {
			padding: 0 0 0 20px;
			height: 60px;
			line-height: 60px;
			float: left;
			color: #fff;
			font-size: 24px;
		}

		.nav-user-photo {
			width: 36px;
			height: 36px;
			margin: 8px 8px 0 0;
			border-radius: 100%;
			border: 2px solid #FFF;
		}

		.nav-header {
			float: right;
			height: 100%;
			width: auto;
			color: #fff;
		}

		.nav-header > ul > li {
			float: right;
			height: 60px;
			padding: 5px 10px;
		}

		.nav-header > ul > li:hover {
			cursor: pointer;
		}

		#nav-first {
			margin: 0 20px 0 0;
		}

		.nav_head_font {
			padding-left: 15px;
		}

		.light-blue {
			background: #579EC8 !important
		}

		#logintime {
			font-size: 14px;
			margin-left: 25px;
		}
	</style>
</head>

<script type="text/javascript">

</script>
<body>
<div id="frame-header">
	<div id="frame-banner">
		<i class="fa fa-leaf"></i>
		<span> ${sysName}</span>
		<span id="logintime">
        <#if Session.USER??>
            <#if Session.USER.loginTime??>
				上次访问时间 ${Session.USER.loginTime?string("yyyy年MM月dd日 HH时mm分ss秒")}
            <#else >
            ${(Session.USER.loginTime)!"这是您第一次登录哟！"}
            </#if>
        <#else>
				您还没有通过权限验证……！
        </#if>
		</span>
	</div>
	<div class="nav-header">
		<ul class="nav nav-pills">
			<li id="nav-first" class="dropdown light-blue">
				<div class="dropdown-toggle" data-toggle="dropdown">
					<img class="nav-user-photo" src="${webRoot}/${(Session.USER.headPic)!'upload/default.png'}" alt=""/>
					<small>
                    <#if Session.USER??>
						欢迎光临,
                    <#else >
						请登录……
                    </#if>
					</small>
                ${(Session.USER.realName)!}
					<span class="caret"></span>
				</div>
				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="#" id="setting">
							<i class="fa fa-gear"></i>
							<span class="nav_head_font">设置</span>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="#" id="info">
							<i class="fa fa-user"></i>
							<span class="nav_head_font">个人资料</span>
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a id="exit">
							<i class="fa fa-power-off"></i>
							<span class="nav_head_font">退出</span>
						</a>
					</li>
				</ul>
			</li>

		</ul>
	</div>
</div>
<div id="frame-body">
	<div id="frame-menu">
		<div class="menu-tree">
			<ul id="nav"></ul>
		</div>
		<div class="menu-hide"></div>
	</div>
	<div id="frame-content">
		<div id="nav_content"></div>
		<iframe id="content" src="https://www.baidu.com"></iframe>
	</div>
</div>
<div id="frame-footer"></div>
</body>

<script type="text/javascript" src="${webRoot}/js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="${webRoot}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${webRoot}/layui/layui.js"></script>

<script>
	$(function () {
		var main_height = function () {
			$("body").css("height", $(window).height());
		};
		main_height();

		/*窗体大小改变时，修改主体的高度*/
		$(window).resize(function () {
			main_height();
		});

		layui.use(["laypage", "layer", "tree"], function () {
			var layer = layui.layer;

			$("#exit").click(function () {
				layer.confirm("确定要退出系统吗？", {icon: 3, title: '提示', offset: '200px'}, function (index) {
					window.location.href = "${webRoot}/user/doLogout";
					layer.close(index);
				});
			});

			layui.tree({
				elem: "#nav",
				click: function (item) { //点击节点回调
					layer.msg('当前节名称：' + item.name + '<br>全部参数：' + JSON.stringify(item));
					console.log(item);
				},
				target: "_blank",
				nodes: [ //节点
					{
						name: '综合业务'
						, id: 1
						, alias: 'changyong'
						, children: [
						{
							name: '所有未读（设置跳转）'
							, id: 11
							, href: 'http://www.layui.com/'
							, alias: 'weidu'
						}, {
							name: '置顶邮件'
							, id: 12
						}, {
							name: '标签邮件'
							, id: 13
						}
					]
					},
					{
						name: '系统管理'
						, id: 2
						, spread: false
						, children: [
						{name: '用户管理', id: 21},
						{name: '角色管理', id: 22}
					]
					}
					, {
						name: '收藏夹'
						, id: 3
						, alias: 'changyong'
						, children: [
							{
								name: '爱情动作片'
								, id: 31
								, alias: 'love'
							}, {
								name: '技术栈'
								, id: 12
								, children: [
									{
										name: '前端'
										, id: 121
									}
									, {
										name: '全端'
										, id: 122
									}
								]
							}
						]
					}
				]
			});
			$("#info").click(function () {
				layer.open({
					type: 2,
					title: 'layer mobile页',
					shade: 0.8,
//                    time:6000,
					shadeClose: true,
					area: ['380px', '90%'],
					content: 'http://layer.layui.com/mobile/' //iframe的url
				});
			});
		});


	})

</script>
</html>