package com.zrzy.dao;

import com.zrzy.entity.CourseSystem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lenovo on 2017/3/9.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
public class CourseSystemDaoTest {
    @Resource
    private CourseSystemDao courseSystemDao;
    @Test
    public void queryCourseSystem() throws Exception {
        List<CourseSystem> courseSystems = courseSystemDao.queryCourseSystem();
        System.out.println(courseSystems);

    }

    @Test
    public void insertCourseSystem() throws Exception {

    }

    @Test
    public void updateCourseSystem() throws Exception {

    }

    @Test
    public void deleteCourseSystemById() throws Exception {

    }

}