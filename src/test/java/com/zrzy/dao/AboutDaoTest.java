package com.zrzy.dao;

import com.zrzy.entity.About;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/09 16:04
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
//@Transactional
//@Rollback
public class AboutDaoTest {
    @Resource
    private AboutDao aboutDao;

    @Test
    public void insert() throws Exception {
        About about = new About();
        about.setTitle("dfdfdf");
        about.setClassify(1);
        aboutDao.insert(about);
    }

    @Test
    public void deleteById() throws Exception {
        aboutDao.deleteById(Long.parseLong("8"));

    }

    @Test
    public void updateById() throws Exception {
       About about=new About();
       about.setId(Long.parseLong("7"));
       about.setTitle("women");
      aboutDao.updateById(about);
    }

    @Test
    public void findByClassify() throws Exception {
     System.out.println( aboutDao.findByClassify(1));
    }

}